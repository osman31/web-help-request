let users = [];
let ticketId;


async function fetchText(url) {
    let response = await fetch(url);
    let data = await response.json();

    return data;
}

async function afficherUser() {
    const usersdata = await fetchText(
        "https://web-help-request-api.herokuapp.com/users"
    );
    users = usersdata.data;
    document.getElementById("user").innerHTML = null;
    for (let i = 0; i < usersdata.data.length; i++) {
        document.getElementById(
            "user"
        ).innerHTML += `<option value="${usersdata.data[i].id}">${usersdata.data[i].username}</option>`;
    }

    afficherTicket();
}

afficherUser();

async function afficherTicket() {
    const ticketsdata = await fetchText(
        "https://web-help-request-api.herokuapp.com/tickets"
    );
    ticketId = ticketsdata.data[0].id
    console.log(ticketsdata.data);
    document.getElementById("table").innerHTML = null;
    for (let i = 0; i < ticketsdata.data.length; i++) {
        const user = users.filter((e) => e.id === ticketsdata.data[i].users_id)[0]
            .username;
        document.getElementById("table").innerHTML += ` <tr>
    <th scope="row">${ticketsdata.data[i].id}</th>
    <td>${user}</td>
    <td>${ticketsdata.data[i].subject}</td>
    <td>${ticketsdata.data[i].date}</td>
    <td><button type="button" onclick="delTicket(${ticketsdata.data[i].id})"  class="btn btn-danger" value=''>Delete</button></td>
  </tr>`;
    }

}

async function deleteTicket() {
    console.log(ticketId)
    await fetch(`https://web-help-request-api.herokuapp.com/tickets/${ticketId}`, {

        method: 'PATCH'
    });
    location.reload();
}

async function delTicket(delId) {
    deleteid = delId
    await fetch(`https://web-help-request-api.herokuapp.com/tickets/${deleteid}`, {

        method: 'PATCH'
    });
    location.reload();
}

function createticket() {
    usrid = document.getElementById('user').value
    usrSubject = document.getElementById('inputText').value
    console.log(usrSubject)
    console.log(usrid)
    fetch("https://web-help-request-api.herokuapp.com/tickets", {
        method: 'POST',
        body: new URLSearchParams({
            subject: usrSubject,
            userId: usrid
        })
    })

}